package com.transvoyant.tvideaplugin;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.diagnostic.Logger;
import org.jetbrains.annotations.NotNull;

import static com.transvoyant.tvideaplugin.GrepConsoleUtils.registerGrepConsoleFunction;


public class TvIdeaPlugin implements ApplicationComponent {
    private static final Logger LOG = Logger.getInstance(TvIdeaPlugin.class);

    public static final String TV_IDEA_PLUGIN = "TvIdeaPlugin";

    private LogFormatter logFormatter = new LogFormatter();

    @Override
    public void initComponent() {
        try {
            registerGrepConsoleFunction("tvLogFormat", logFormatter::formatLogLine);
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    @Override
    public void disposeComponent() {

    }

    @NotNull
    @Override
    public String getComponentName() {
        return TV_IDEA_PLUGIN;
    }
}
