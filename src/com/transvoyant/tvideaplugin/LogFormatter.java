package com.transvoyant.tvideaplugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import static com.transvoyant.tvideaplugin.NotificationService.notifyDuplicatedRecord;

public class LogFormatter {
    private static final String DUPLICATED_RECORD_COUNT = "duplicate record found";

    private JsonParser parser = new JsonParser();
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public String formatLogLine(String logLine) {
        if (logLine.contains(DUPLICATED_RECORD_COUNT)) {
            notifyDuplicatedRecord();
        }

        if (logLine.startsWith("{")) {
            JsonElement el = parser.parse(logLine);
            logLine = gson.toJson(el);
        }

        return logLine;
    }
}
