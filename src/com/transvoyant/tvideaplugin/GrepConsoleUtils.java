package com.transvoyant.tvideaplugin;

import com.intellij.ide.plugins.IdeaPluginDescriptorImpl;
import com.intellij.ide.plugins.PluginManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.extensions.PluginId;
import com.intellij.openapi.progress.ProcessCanceledException;

import java.util.function.Function;

import static com.transvoyant.tvideaplugin.NotificationService.notifyPluginWarning;

public class GrepConsoleUtils {
    private static final Logger LOG = Logger.getInstance(GrepConsoleUtils.class);

    /**
     * @param functionName
     * @param function
     *       - The text will never be empty, it may or may not end with a newline - \n
     *       - It is possible that the stream will flush prematurely and the text will be incomplete: IDEA-70016
     *       - Return null to remove the line
     *       - Processing blocks application output stream, make sure to limit the length and processing time when needed using #limitAndCutNewline
     */
    public static void registerGrepConsoleFunction(String functionName, Function<String, String> function) {
        try {
            IdeaPluginDescriptorImpl descriptor = (IdeaPluginDescriptorImpl) PluginManager.getPlugin(PluginId.getId("GrepConsole"));
            Class<?> clazz = descriptor.getPluginClassLoader().loadClass("krasa.grepconsole.plugin.ExtensionManager");

            clazz.getMethod("registerFunction", String.class, Function.class)
                    .invoke(null, functionName, new Function<String, String>() {

                        @Override
                        public String apply(String s) {
                            try {
                                return function.apply(s);
                            } catch (ProcessCanceledException ex) {
                                notifyPluginWarning("Log line processing took too long for: " + s);
                            }
                            return s;
                        }
                    });

            LOG.info("'" + functionName + "' registered");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
