package com.transvoyant.tvideaplugin;

import com.intellij.notification.NotificationGroup;
import com.intellij.notification.Notifications.Bus;
import com.intellij.openapi.application.ApplicationManager;

import static com.intellij.notification.NotificationDisplayType.BALLOON;
import static com.intellij.openapi.ui.MessageType.ERROR;
import static com.intellij.openapi.ui.MessageType.WARNING;

public class NotificationService {
    private static final NotificationGroup NOTIFICATION = new NotificationGroup(TvIdeaPlugin.TV_IDEA_PLUGIN, BALLOON, true);

    public static void notifyPluginWarning(String msg) {
        ApplicationManager.getApplication().invokeLater(() -> {
            Bus.notify(NOTIFICATION.createNotification(msg, WARNING));
        });
    }

    public static void notifyDuplicatedRecord() {
        ApplicationManager.getApplication().invokeLater(() -> {
            Bus.notify(NOTIFICATION.createNotification("duplicate record found", ERROR));
        });
    }
}
